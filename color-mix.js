import { mix } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorMix extends HTMLElement {
  constructor() {
    super();
  }

  set target(value) {
    this.reflect("target", value);
  }

  get target() {
    return this.getAttribute("target");
  }

  set strength(value) {
    this.reflect("strength", value);
  }

  get strength() {
    return this.getAttribute("strength");
  }

  set steps(value) {
    this.reflect("steps", value);
  }

  get steps() {
    return this.getAttribute("steps");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["target", "strength", "steps"];
  }

  connectedCallback() {
    const target = this.target || "#000000";
    const [strength, steps] = ["strength", "steps"].map(
      (prop) => Number(this[prop]) || 0,
    );

    function colors(tree) {
      return Array.from(tree).forEach((node) => {
        if (node.swatch) {
          if (steps) {
            const container = document.createElement("div");

            const interpolated = mix({ target, strength, steps }, node.swatch);

            interpolated.forEach((color, index) => {
              const el = document.createElement(node.nodeName.toLowerCase());
              el.swatch = color;
              el.format = node.format;
              el.id = node.id ? [node.id, index].join("-") : undefined;

              container.append(el);

              node.replaceWith(container);
            });

            container.replaceWith(...container.children);
          } else {
            node.swatch = mix({ target, strength }, node.swatch);
          }
        }

        return colors(node.children);
      });
    }

    colors(this.children);
    this.replaceWith(...this.children);
  }
}

if (!customElements.get("color-mix")) {
  customElements.define("color-mix", ColorMix);
}
