import { palette } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

function template(configuration, node, palette) {
  const tmpl = document.createElement("template");

  const surface = Object.entries(palette).filter(([key]) =>
    ["bg", "fg"].some((v) => key === v)
  );

  const main = Object.entries(palette).filter(
    ([key]) => key.endsWith("0") && !key.startsWith("a"),
  );

  const accents = Object.entries(palette).filter(
    ([key]) => key.endsWith("0") && key.startsWith("a"),
  );

  const variants = Object.entries(palette).filter(([key]) =>
    ["light", "muted", "dark", "accent", "state"].some((v) => key === v)
  );

  function render(group, colors, ...keys) {
    return colors.length
      ? `
<div class="${group}">
${
        colors
          .map(([key, color]) =>
            node.id
              ? `
      <color-token id="${
                [node.id, ...keys, key].join(
                  "-",
                )
              }" swatch="${color}" format="${node.format}"></color-token>
      `
              : `
      <color-token swatch="${color}" format="${node.format}"></color-token>
      `
          )
          .join("\n")
      }
</div>
`
      : ``;
  }

  tmpl.innerHTML = `
<div class="palette ${configuration}">
${render("surface", surface)}
${main && render("main", main)}
${accents && render("accents", accents)}
${
    variants
      .map(([key, data]) => render(key, Object.entries(data), key))
      .join("\n")
  }
</div>
`;

  return tmpl.content.cloneNode(true);
}

export class ColorPalette extends HTMLElement {
  constructor() {
    super();

    let data = [];

    this.render(data);
  }

  set configuration(value) {
    this.reflect("configuration", value);
  }

  get configuration() {
    return this.getAttribute("configuration");
  }

  set contrast(value) {
    this.reflect("contrast", value);
  }

  get contrast() {
    return this.getAttribute("contrast");
  }

  set accents(_value) {
    this.reflect("accents", true);
  }

  get accents() {
    return this.getAttribute("accents");
  }

  set dark(_value) {
    this.reflect("dark", true);
  }

  get dark() {
    return this.getAttribute("dark");
  }

  // Material specific

  set states(_value) {
    this.reflect("states", true);
  }

  get states() {
    return this.getAttribute("states");
  }

  // Artistic specific

  set tints(value) {
    this.reflect("tints", value);
  }

  get tints() {
    return this.getAttribute("tints");
  }

  set tones(value) {
    this.reflect("tones", value);
  }

  get tones() {
    return this.getAttribute("tones");
  }

  set shades(value) {
    this.reflect("shades", value);
  }

  get shades() {
    return this.getAttribute("shades");
  }

  // Data flags

  set filtered(_value) {
    this.reflect("filtered", true);
  }

  get filtered() {
    return this.getAttribute("filtered");
  }

  set simulated(_value) {
    this.reflect("simulated", true);
  }

  get simulated() {
    return this.getAttribute("simulated");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  render(data) {
    const configuration = this.configuration || "material";
    const contrast = Number(this.contrast) || 100;
    const [accents, dark] = ["accents", "dark"].map(
      (prop) => this[prop] === "" || false,
    );

    const states = this.states === "" || false;

    const [tints, tones, shades] = ["tints", "tones", "shades"].map((prop) => {
      const value = this[prop] || 3;

      return Number(value);
    });

    globalThis.addEventListener("DOMContentLoaded", (evt) => {
      Array.from(this.children).forEach((node, index) => {
        data[index] = palette(
          {
            configuration,
            contrast,
            accents,
            dark,
            ...((configuration === "artistic" && {
              tints,
              tones,
              shades,
            }) || {
              states,
            }),
          },
          node.swatch,
        );

        this.color = node.id;
        this.format = node.format || "hex rgb hsl";

        node.replaceWith(
          template(
            configuration,
            {
              id: this.color,
              format: this.format,
            },
            data[index],
          ),
        );
      });

      this.data = data.length === 1 ? data[0] : data;
    });
  }

  static get observedAttributes() {
    return [
      "configuration",
      "contrast",
      "accents",
      "dark",
      "states",
      "tints",
      "tones",
      "shades",
      "filtered",
      "simulated",
    ];
  }

  attributeChangedCallback(name) {
    if (name === "filtered" || name === "simulated") {
      const configuration = this.configuration || "material";

      this.replaceChildren(
        template(
          configuration,
          { id: this.color, format: this.format },
          this.data,
        ),
      );
    }
  }
}

if (!customElements.get("color-palette")) {
  customElements.define("color-palette", ColorPalette);
}
