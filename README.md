# QuarkSuite Declarative (WIP)

## Summary

This is the source repository of QuarkSuite Declarative. It's a translation layer over the functionality of
[Quarksuite Core](https://codeberg.org/quarksuite/core) that allows you to create and export tokens in a
declarative HTML workspace.

The tokens themselves are rendered as web components provided by [Quarksuite Elements](https://codeberg.org/quarksuite/elements).
