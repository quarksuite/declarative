import { convert } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorConvert extends HTMLElement {
  constructor() {
    super();
  }

  set to(value) {
    this.reflect("to", value);
  }

  get to() {
    return this.getAttribute("to");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["to"];
  }

  connectedCallback() {
    const to = this.to || "hex";

    function colors(tree) {
      return Array.from(tree).forEach((node) => {
        if (node.swatch) {
          node.swatch = convert(to, node.swatch);
        }

        return colors(node.children);
      });
    }

    colors(this.children);
    this.replaceWith(...this.children);
  }
}

if (!customElements.get("color-convert")) {
  customElements.define("color-convert", ColorConvert);
}
