import { adjust } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorAdjust extends HTMLElement {
  constructor() {
    super();
  }

  set lightness(value) {
    this.reflect("lightness", value);
  }

  get lightness() {
    return this.getAttribute("lightness");
  }

  set chroma(value) {
    this.reflect("chroma", value);
  }

  get chroma() {
    return this.getAttribute("chroma");
  }

  set hue(value) {
    this.reflect("hue", value);
  }

  get hue() {
    return this.getAttribute("hue");
  }

  set alpha(value) {
    this.reflect("alpha", value);
  }

  get alpha() {
    return this.getAttribute("alpha");
  }

  set steps(value) {
    this.reflect("steps", value);
  }

  get steps() {
    return this.getAttribute("steps");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["lightness", "chroma", "hue", "alpha", "steps"];
  }

  connectedCallback() {
    const [lightness, chroma, hue, alpha, steps] = [
      "lightness",
      "chroma",
      "hue",
      "alpha",
      "steps",
    ].map((prop) => Number(this[prop]) || 0);

    function colors(tree) {
      return Array.from(tree).forEach((node) => {
        if (node.swatch) {
          if (steps) {
            const container = document.createElement("div");

            const interpolated = adjust(
              { lightness, chroma, hue, alpha, steps },
              node.swatch,
            );

            interpolated.forEach((color, index) => {
              const el = document.createElement(node.nodeName.toLowerCase());
              el.swatch = color;
              el.format = node.format;
              el.id = node.id ? [node.id, index].join("-") : undefined;

              container.append(el);

              node.replaceWith(container);
            });

            container.replaceWith(...container.children);
          } else {
            node.swatch = adjust(
              { lightness, chroma, hue, alpha },
              node.swatch,
            );
          }
        }

        return colors(node.children);
      });
    }

    colors(this.children);
    this.replaceWith(...this.children);
  }
}

if (!customElements.get("color-adjust")) {
  customElements.define("color-adjust", ColorAdjust);
}
