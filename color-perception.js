import { perception } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorPerception extends HTMLElement {
  constructor() {
    super();
  }

  set check(value) {
    this.reflect("check", value);
  }

  get check() {
    return this.getAttribute("check");
  }

  set severity(value) {
    this.reflect("severity", value);
  }

  get severity() {
    return this.getAttribute("severity");
  }

  // Check vision

  set as(value) {
    this.reflect("as", value);
  }

  get as() {
    return this.getAttribute("as");
  }

  set method(value) {
    this.reflect("method", value);
  }

  get method() {
    return this.getAttribute("method");
  }

  // Check contrast sensitivity

  set factor(value) {
    this.reflect("factor", value);
  }

  get factor() {
    return this.getAttribute("factor");
  }

  set K(value) {
    this.reflect("K", value);
  }

  get K() {
    return this.getAttribute("K");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["check", "as", "method", "factor", "K", "severity"];
  }

  connectedCallback() {
    const check = this.check || "vision";
    const severity = Number(this.severity) || 50;

    const as = this.as || "protanopia";
    const method = this.method || "brettel";

    const factor = Number(this.factor) || 0;

    const K = Number(this.K) || 1850;

    globalThis.addEventListener("DOMContentLoaded", (evt) => {
      Array.from(this.children).forEach((node) => {
        node.data = perception(
          {
            check,
            as,
            method,
            severity,
            ...(check === "contrast" && { factor }),
            ...(check === "illuminant" && { K }),
          },
          node.data,
        );

        node.setAttribute("simulated", "");
      });

      this.replaceWith(...this.children);
    });
  }
}

if (!customElements.get("color-perception")) {
  customElements.define("color-perception", ColorPerception);
}
