import { a11y } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorA11y extends HTMLElement {
  constructor() {
    super();
  }

  set mode(value) {
    this.reflect("mode", value);
  }

  get mode() {
    return this.getAttribute("mode");
  }

  // WCAG standard settings

  set rating(value) {
    this.reflect("rating", value);
  }

  get rating() {
    return this.getAttribute("rating");
  }

  set large(_value) {
    this.reflect("large", true);
  }

  get large() {
    return this.getAttribute("large");
  }

  // Colorimetric custom settings

  set min(value) {
    this.reflect("min", value);
  }

  get min() {
    return this.getAttribute("min");
  }

  set max(value) {
    this.reflect("max", value);
  }

  get max() {
    return this.getAttribute("max");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["mode", "rating", "large", "min", "max"];
  }

  connectedCallback() {
    const mode = this.mode || "standard";

    const rating = this.rating || "AA";
    const large = this.large === "" || false;

    const min = Number(this.min) || 85;
    const max = Number(this.max) || undefined;

    globalThis.addEventListener("DOMContentLoaded", (evt) => {
      Array.from(this.children).forEach((node) => {
        node.data = a11y(
          {
            mode,
            rating,
            large,
            ...(mode === "custom" && { min, max }),
          },
          node.data,
        );
        node.setAttribute("filtered", "");
      });

      this.replaceWith(...this.children);
    });
  }
}

if (!customElements.get("color-a11y")) {
  customElements.define("color-a11y", ColorA11y);
}
