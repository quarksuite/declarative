import { harmony } from "https://cdn.jsdelivr.net/npm/@quarksuite-two/core@2.2.0/color.js";

export class ColorHarmony extends HTMLElement {
  constructor() {
    super();
  }

  set configuration(value) {
    this.reflect("configuration", value);
  }

  get configuration() {
    return this.getAttribute("configuration");
  }

  set accented(_value) {
    this.reflect("accented", true);
  }

  get accented() {
    return this.getAttribute("accented");
  }

  reflect(name, value) {
    if (value) {
      this.setAttribute(name, value);
    } else {
      this.removeAttribute(name);
    }
  }

  static get observedAttributes() {
    return ["to"];
  }

  connectedCallback() {
    const configuration = this.configuration || "complementary";
    const accented = this.accented === "" || false;

    function colors(tree) {
      return Array.from(tree).forEach((node) => {
        if (node.swatch) {
          const container = document.createElement("div");

          const harmonized = harmony({ configuration, accented }, node.swatch);

          harmonized.forEach((color, index) => {
            const categories = ["a", "b", "c", "d"];

            const el = document.createElement(node.nodeName.toLowerCase());
            el.swatch = color;
            el.format = node.format;
            el.id = node.id
              ? [node.id, categories[index]].join("-")
              : undefined;

            container.append(el);

            node.replaceWith(container);
          });

          container.replaceWith(...container.children);
        }

        return colors(node.children);
      });
    }

    colors(this.children);
    this.replaceWith(...this.children);
  }
}

if (!customElements.get("color-harmony")) {
  customElements.define("color-harmony", ColorHarmony);
}
